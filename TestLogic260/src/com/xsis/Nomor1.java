package com.xsis;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Nomor1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        List<Integer> arrSatu = new ArrayList<>();
        List<Integer> arrDua = new ArrayList<>();
        int[] arrJumlah = new int[n];

        //Array deret ke 1
        int i = 0;
        while (arrSatu.size() < n) {
            if ((i) % 3 == 0) {
                arrSatu.add((i) - 1);
            }
            i++;
        }

        //Array deret ke 2
        i = 0;
        while (arrDua.size() < n) {
            if ((i) % -2 == 0) {
                arrDua.add((i) * 1);
            }
            i++;
        }

        //Array deret jumlah
        for (int j = 0; j < n; j++) {
            arrJumlah[j] = (arrSatu.get(j) + arrDua.get((j)));
        }

        //System.out.println(arrSatu);
        //System.out.println(arrDua);

        for (int j = 0; j < arrSatu.size(); j++) {
            if(arrSatu.get(j) % 5 == 0){
                System.out.print("! ");
            } else {
                System.out.print(arrSatu.get(j) + " ");
            }
        }

        System.out.println();

        for (int j = 0; j < arrDua.size(); j++) {
            if(arrDua.get(j) % 5 == 0){
                System.out.print(" ! ");
            } else {
                System.out.print(arrDua.get(j) + " ");
            }
        }

        System.out.println();

        for (int j = 0; j < arrJumlah.length; j++) {
            System.out.print(arrJumlah[j] + " ");
        }
        //System.out.println(arrJumlah);
    }
}

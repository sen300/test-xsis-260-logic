package com.xsis;

import java.util.Scanner;

public class Nomor8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String kalimat = input.nextLine();
        String[] kataArray = kalimat.split(" ");
        String result = "";

        for (int i = 0; i < kataArray.length; i++) {
            char[] karakterArray = kataArray[i].toCharArray();
            boolean censored = false;
            for (int j = 0; j < karakterArray.length; j++) {
                //selama bukan awal dan akhir akan nambah ke string sebagai *
                if (j != 0 && j != (karakterArray.length-1)) {
                    if (!censored) {
                        result += "***";
                        censored = true;
                    }
                } else {
                    result += karakterArray[j];
                }
            }
            result += " ";
        }

        System.out.println(result);
    }
}

package com.xsis;

import java.util.Scanner;

public class Nomor2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[] jarakArray = {0.5, 1.5, 1.5, 1.5};
        String ruteString = input.nextLine();
        String[] ruteArray = ruteString.split(String.valueOf('-'));
        int[] rute = convertArrayStringToInt(ruteArray);
        double jarak = 0;
        //bambang ditoko 10 menit tiap toko, kecepatan 30 km/ jam

        //set jarak awal
        jarak = moveWay(-1, (rute[0]), jarakArray);
        //System.out.println(jarak);
        for (int i = 0; i < (rute.length - 1); i++) {
            if (rute[i] < rute[i + 1]) {
                //System.out.println("tes 1");
                jarak += moveWay(rute[i] - 1, rute[i + 1], jarakArray);

            } else if (rute[i] > rute[i + 1]) {
                //System.out.println("tes 2");
                jarak += moveWay(rute[i + 1] - 1, rute[i], jarakArray);
            }
        }
        System.out.println();

        //30km/jam ==> 1km/2menit
        int waktu = 2;

        double jarakTotal = (jarak + moveWay(-1, rute[rute.length - 1], jarakArray));
        System.out.println("jarak total: " + jarakTotal + " KM");

        //tiap toko 10 menit jadi kali jumlah toko dengan 10 menit
        double waktuMenit = (jarakTotal * waktu) + (rute.length * 10);
        System.out.println(("waktu jalan: " + waktuMenit + " Menit"));
    }

    public static double moveWay(int awal, int akhir, double[] jarak) {
        double jumlah = 0;
        for (int i = (akhir - 1); i > awal; i--) {
            jumlah += jarak[i];
        }
        return jumlah;
    }

    public static int[] convertArrayStringToInt(String[] numberArray){
        int[] numbers = new int[numberArray.length];

        for (int i = 0; i < numberArray.length; i++) {
            numbers[i] = Integer.parseInt(numberArray[i]);
        }

        return numbers;
    }
}

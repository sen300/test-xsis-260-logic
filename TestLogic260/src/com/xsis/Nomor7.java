package com.xsis;

import java.util.Scanner;

public class Nomor7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = Integer.parseInt(input.nextLine());
        int y = Integer.parseInt(input.nextLine());
        String[] z = input.nextLine().split(" ");
        //output  8 Maret 2020 = 12 hari (2020 kabisat)
        //kpk 3 dan 2 adalah 6 dan 12, kenapa yang dipilih 12?

        int tgl_mulai = Integer.parseInt(z[0]);
        int tgl_selanjutnya = 0;
        //boolean ketemu = false;
        int i = 0;

        x += 1;
        y += 1;

        int kpk = 0;
        if (x > y) {
            kpk = x;
        } else {
            kpk = y;
        }

        while(true) {
            if (kpk % x  == 0 && kpk % y == 0) {
                tgl_selanjutnya = kpk;
               break;
            }
            kpk++;
        }

        System.out.println(tgl_selanjutnya + " Hari");

        int tgl_libur = tgl_mulai + tgl_selanjutnya;
        int bulanAwal = bulan(z[1]);

        int bulanHari = 0;
        if (bulanAwal % 2 == 0) {
            if (bulanAwal == 2) {
                if (Integer.parseInt(z[2]) % 4 == 0)
                    bulanHari = 29;
                else
                    bulanHari = 28;
            }

            else
                bulanHari = 30;
        } else {
            bulanHari = 31;
        }

        if (tgl_libur > bulanHari) {
            tgl_libur = tgl_libur % bulanHari;
            bulanAwal++;
            System.out.println(tgl_libur + " " + angkaKeBulan(bulanAwal) + " " + z[2]);
        } else {
            System.out.println(tgl_libur + z[1] + z[2]);
        }

    }

    public static int bulan (String bulan) {
        switch (bulan) {
            case "Januari":
                return  1;
            case "Februari":
                return  2;
            case "Maret":
                return  3;
            case "April":
                return  4;
            case "Mei":
                return  5;
            case "Juni":
                return  6;
            case "Juli":
                return  7;
            case "Agustus":
                return  8;
            case "September":
                return  9;
            case "Oktober":
                return  10;
            case "November":
                return  11;
            case "Desember":
                return  12;
        }
        return 0;
    }

    public static String angkaKeBulan (int bulan) {
        switch (bulan) {
            case 1:
                return  "Januari";
            case 2:
                return  "Februari";
            case 3:
                return  "Maret";
            case 4:
                return  "April";
            case 5:
                return  "Mei";
            case 6:
                return  "Juni";
            case 7:
                return  "Juli";
            case 8:
                return  "Agustus";
            case 9:
                return  "September";
            case 10:
                return  "Oktober";
            case 11:
                return  "November";
            case 12:
                return  "Desember";
        }
        return "bulan tidak ketemu";
    }
}

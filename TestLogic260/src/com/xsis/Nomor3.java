package com.xsis;

import java.util.*;

public class Nomor3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String textInput = input.nextLine();
        textInput = textInput.replace(" ", "");
        String[] textArr = textInput.split(":|,");
        //tree map biar berurutan dibanding hashmap yang tidak
        Map<String, Integer> buahMap = new TreeMap<>();

        for (int i = 0; i < textArr.length; i += 2) {
            int angka = Integer.parseInt(textArr[i + 1]);
            buahMap.computeIfPresent(textArr[i], (k, v) -> v + angka);
            buahMap.putIfAbsent(textArr[i], angka);
        }

        System.out.println("Summary penjualan, dalam alphabetical order:");
        for (Map.Entry<String, Integer> buah : buahMap.entrySet()) {
            System.out.println(buah.getKey() + ": " + buah.getValue());
        }
    }
}

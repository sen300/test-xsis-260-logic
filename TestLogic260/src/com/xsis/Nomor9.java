package com.xsis;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Nomor9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String textInput = input.next().toLowerCase(Locale.ROOT);
        char[] textInputArr = textInput.toCharArray();
        int[] arrInput = new int[textInputArr.length];

        for (int i = 0; i < arrInput.length; i++) {
            arrInput[i] = input.nextInt();
        }

        String[] result = new String[textInputArr.length];
        for (int i = 0; i < result.length; i++) {
            if( (textInputArr[i] - 96) == arrInput[i]) {
                result[i] = "True";
            } else {
                result[i] = "False";
            }
        }

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + ", ");
        }
    }
}

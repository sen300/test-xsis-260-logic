package com.xsis;

import java.util.Scanner;

public class Nomor5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String waktuInput = input.nextLine();

        convertWaktu(waktuInput);
    }

    public static void convertWaktu(String waktuString) {
        //ambil angka jam (2 karakter didepan)
        String hour = waktuString.substring(0, 2);
        //ambil menit (karakter selain hour dan sebelum keterangn AM/PM)
        String minute = waktuString.substring(2, 5);
        int hourInt = Integer.parseInt(hour);

        //cek kalo panjang kurang dari 6 berarti dia formatnya XX:XX bukan pake am/pm
        if (waktuString.length() < 6) {
            String ampm = "";
            if (hourInt >= 12) {
                if (hourInt != 12) hour = String.format("%02d", (hourInt - 12));
                else hour = String.format("%02d", (hourInt));
                ampm = " PM";
            } else {
                if (hourInt == 0) hour = "12";
                ampm = " AM";
            }
            System.out.println(hour + minute + ampm);
        } else {
            //ambil keterangan ampm (2 karakter terakhir)
            String ampm = waktuString.substring(waktuString.length()-2);
            if (ampm.equalsIgnoreCase("PM")) {
                if (hourInt != 12) hour = String.format("%02d", (hourInt + 12));
                else hour = String.format("%02d", (hourInt));
            } else {
                if (hourInt == 12) hour = "00";
            }


            System.out.println(hour + minute );
        }
    }
}

package com.xsis;

import java.util.*;

public class Nomor6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String textInput = input.nextLine().toLowerCase();
        textInput = textInput.replace(" ", "");
        char[] textCharArr = textInput.toCharArray();

        List<Character> vokal = new ArrayList<>();
        List<Character> konsonan = new ArrayList<>();

        for (int i = 0; i < textCharArr.length; i++) {
            //kalo char sama dengan vokal maka masuk ke vokal dan sebaliknya
            if (textCharArr[i] == 97 || textCharArr[i] == 101 || textCharArr[i] == 105 || textCharArr[i] == 111 || textCharArr[i] == 117 ) {
                vokal.add(textCharArr[i]);
            } else {
                konsonan.add(textCharArr[i]);
            }
        }

        Collections.sort(vokal);
        Collections.sort(konsonan);

        System.out.println("Vokal: ");
        for (int i = 0; i < vokal.size(); i++) {
            System.out.print(vokal.get(i));
        }
        System.out.println();
        System.out.println("Konsonan: ");
        for (int i = 0; i < konsonan.size(); i++) {
            System.out.print(konsonan.get(i));
        }
    }
}

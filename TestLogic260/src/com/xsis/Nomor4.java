package com.xsis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Nomor4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Uang Andi : ");
        int duit = Integer.parseInt(input.nextLine());
        System.out.println("Jumlah Barang: ");
        int jmlBarang = Integer.parseInt(input.nextLine());

        String[] barangJualan = new String[jmlBarang];
        int[] hargaJualan = new int[jmlBarang];

        for (int i = 0; i < jmlBarang; i++) {
            System.out.println("Masukan Nama Barang ke "+ (i+1) +": ");
            barangJualan[i] = input.nextLine();
            System.out.println("Masukan Harga Barang"+ (i+1) +": ");
            hargaJualan[i] = Integer.parseInt(input.nextLine());
        }

        //sorting bubble
        for (int i = 0; i < hargaJualan.length - 1; i++)
            for (int j = 0; j < hargaJualan.length - i - 1; j++) {
                //n-i-1 dikurang satu karena mulai dari n-1
                //System.out.print(n - i - 1 + " ");
                //kalo sampingnya lebih besar, tukar (asc)
                //kalo sampingnya lebih kecil, tukar (desc)
                //cek apa panjang index1 lebih kecil dari index2.
                //logikanya satuan<puluhan<ratusan<dst. jadi bandingin length

                //ini kalo sama panjangnya baru bandingin
                if ((hargaJualan[j]) > (hargaJualan[j + 1])) {
                    int tempHarga = hargaJualan[j];
                    hargaJualan[j] = hargaJualan[j + 1];
                    hargaJualan[j + 1] = tempHarga;

                    String tempBarang = barangJualan[j];
                    barangJualan[j] = barangJualan[j + 1];
                    barangJualan[j + 1] = tempBarang;
                }
            }

        List<Integer> barangIndex = new ArrayList<>();
        //List<Integer> hargaDibeli = new ArrayList<>();
        int i = 0;
        while (duit >= 0 && i < barangJualan.length) {
            //System.out.println(duit);
            //if (!hargaDibeli.contains(hargaJualan[i])) {
                if (duit >= hargaJualan[i]) {
                    duit -= hargaJualan[i];
                    barangIndex.add(i);
                    //hargaDibeli.add(hargaJualan[i]);
                }
            //}
            i++;
        }

//        for (int j = 0; j < barangJualan.length; j++) {
//            int duitAndi = duit;
//            List<Integer> kombinasi_lama = new ArrayList<>();
//            List<Integer> kombinasi_baru = new ArrayList<>();
//            for (int k = 1; k < barangJualan.length; k++) {
//                if (duit >= hargaJualan[i]) {
//                    duit -= hargaJualan[i];
//                    barangIndex.add(i);
//                    //hargaDibeli.add(hargaJualan[i]);
//                }
//            }

//        }




        //System.out.println(barangIndex);
        String[] result = new String[barangIndex.toArray().length];
        for (i = 0; i < barangIndex.toArray().length; i++) {
            result[i] = barangJualan[barangIndex.get(i)];
        }

        System.out.println("Hasil: ");
        //System.out.println(Arrays.toString(result));

        for (int j = 0; j < result.length; j++) {
            System.out.print(result[j] + ", ");
        }


    }

    public static String[] bubbleSortAscBig(String arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++) {
                //n-i-1 dikurang satu karena mulai dari n-1
                //System.out.print(n - i - 1 + " ");
                //kalo sampingnya lebih besar, tukar (asc)
                //kalo sampingnya lebih kecil, tukar (desc)
                //cek apa panjang index1 lebih kecil dari index2.
                //logikanya satuan<puluhan<ratusan<dst. jadi bandingin length

                    //ini kalo sama panjangnya baru bandingin
                    if (Integer.parseInt(arr[j]) > Integer.parseInt(arr[j + 1])) {
                        String temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;

                }
            }
        return arr;
    }
}
